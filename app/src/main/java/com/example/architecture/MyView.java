package com.example.architecture;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyView extends View {

    private Bitmap mBitmap;

    Collection<Bitmap> BitmapCollection;

    public MyView(Context context) {
        super(context);
        this.setOnTouchListener(new Drawing(this)); // Painting
        BitmapCollection=new ArrayList<Bitmap>();   // Colllection for steps
    }

    public void  setBitmap(Bitmap bitmap)
    {
        mBitmap=bitmap;
    }

    public void AddBitmapToList()
    {
        BitmapCollection.add(mBitmap.copy(mBitmap.getConfig(), true)); // Add current step
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if (mBitmap != null) {
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }
    }
}