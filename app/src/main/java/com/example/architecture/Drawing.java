package com.example.architecture;

import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;

import com.example.architecture.Instruments.Brush;
import com.example.architecture.Instruments.Instrument;

public class Drawing implements View.OnTouchListener {

    private Bitmap mBitmap;
    private MyView mView;
    private float X,Y;

    private static Instrument currentInstrument; //

    public Drawing(MyView mView)
    {
        this.mView=mView;
        currentInstrument=new Brush();
    }

    private void initBitmap()
    {
        mBitmap=Bitmap.createBitmap(mView.getMeasuredWidth(),mView.getMeasuredHeight(), Bitmap.Config.ALPHA_8);
        mView.setBitmap(mBitmap); // Передаем нашей вью какую картинку которую нужно постоянно перерисовывать
    }

    public static void setInstrument(Instrument newInstrument){currentInstrument=newInstrument;} // Changing instrument

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if(mBitmap==null)initBitmap(); /* Для того чтобы получить размеры нашей вью и создать битмап, нужно дождаться пока эта вью будет создана, поэтому
         создание битмап нельзя вызывать в конструкторе Darwing*/

        float x=event.getX();
        float y=event.getY();

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                currentInstrument.setStart(x,y);
                X=x;Y=y;
                break;

            case MotionEvent.ACTION_MOVE:
                currentInstrument.setStart(X,Y);
                currentInstrument.setEnd(x,y);
                X=x;Y=y;
                currentInstrument.DrawSomething(mBitmap);
                break;
            case MotionEvent.ACTION_UP:
                mView.AddBitmapToList();
                break;
        }
        mView.invalidate();
        return true;
    }

}
