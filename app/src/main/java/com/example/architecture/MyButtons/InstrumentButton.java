package com.example.architecture.MyButtons;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.example.architecture.CallBacks.MyHandler;
import com.example.architecture.Instruments.Brush;
import com.example.architecture.Instruments.Instrument;


/* Своя кнопка которая хранит в себе свой инструмент, по ажатию в классе Drawing с помощью статического метода
   изменяется инструмент и продолжаетяс рисование */


public class InstrumentButton extends android.support.v7.widget.AppCompatButton {

    private Instrument currInstr;

    public InstrumentButton(Context context,Instrument i) {
        super(context);
        currInstr=i;

        this.setOnClickListener( new MyHandler(this)); // Listener
    }

    public Instrument getInstrument(){return currInstr;}

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
    }
}
