package com.example.architecture.CallBacks;

import android.view.View;
import android.widget.Button;

import com.example.architecture.Drawing;
import com.example.architecture.MyButtons.InstrumentButton;


public class MyHandler implements View.OnClickListener {

    InstrumentButton b;

    public MyHandler(InstrumentButton b)
    {this.b=b;}

    @Override
    public void onClick(View view) {
        Drawing.setInstrument(b.getInstrument()); //Changing Instrument
    }

}
