package com.example.architecture.Instruments;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

public abstract class Instrument {

    protected Canvas c;
    protected Paint mPaint;
    protected float startX,startY;
    protected float endX,endY;

    public void setPaint(Paint p){};

    // Start point
    public void setStart(float X,float Y)
    {
        startX=X;
        startY=Y;
    }

    // End point
    public void setEnd(float X,float Y) {
        endX=X;
        endY=Y;
    }

    public abstract void DrawSomething(Bitmap mBitmap); // Draw instrument on bitmap

}
