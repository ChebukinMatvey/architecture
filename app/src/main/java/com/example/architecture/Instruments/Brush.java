package com.example.architecture.Instruments;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Brush extends Instrument {

    Path mPath=new Path();

    public Brush()
    {
    }

    @Override
    public void DrawSomething(Bitmap mBitmap) {
        if (mPaint==null)
        {
            mPaint =new Paint();
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.BLACK);
            mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND );
            mPaint.setStrokeWidth(15);
        }
        if(c==null)
            c=new Canvas(mBitmap);
        mPath.moveTo(startX,startY);
        mPath.lineTo(endX,endY);
        c.drawPath(mPath, mPaint);
        mPath.reset();
    }

}
